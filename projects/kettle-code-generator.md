---
layout: page
title: Kettle code generator
permalink: /projects/kettle-code-generator/
exclude_from_nav: true
---
Generates an ANT based project for creating a Pentaho Kettle Step. Helpful for developers who don't want to write boilerplate code. Parameters such as Stepname, datatypes, etc are taken in through a GUI.

*You may need to change the SWT dependency for your Operating system. The default pom comes with SWT for OSX.*

[Source Code](https://github.com/adeydas/kettlecodegen)