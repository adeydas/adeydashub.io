---
layout: page
title: Thought Search
permalink: /projects/thought-search/
exclude_from_nav: true
---
Thought Search was a semantic search engine, developed as a course project for the Semantic Web course. It was capable of natural language processing of unstructured data and store it as a semantic web on which SPARQL queries could be run. This enabled users to get more apt results to queries like "What course do Prof XYZ teach?".

