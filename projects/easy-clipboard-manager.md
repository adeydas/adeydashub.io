---
layout: page
title: Easy Clipboard Manager
permalink: /projects/easy-clipboard-manager/
exclude_from_nav: true
---
Developed during my undergraduate years, Easy Clipboard Manager was an extension to the Windows clipboard manager that enabled many to many copy functions including copying to remote folders via FTP or other transfer protocols. It also built on the then (possibly) slower transfer speed and made it a little better. Developed in C# .NET, it ran on Windows XP and higher.

Below is a screenshot of its interface.
![Easy Clipboard Manager](/projects/project-files/ecm/sc.jpg)