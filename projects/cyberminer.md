---
layout: page
title: Cyberminer
permalink: /projects/cyberminer/
exclude_from_nav: true
---
Cyberminer was a KWICK (Key word in context) based search engine developed as part of a course project for Advanced Software Architecture and Design.

A presentation describing the system is on [Prezi](https://prezi.com/ym5gpflk0fte/cyberminer/#). Apart from that, a set of documents describing the project are below.

* [Architecture document](/projects/project-files/cyberminer/Architecture_Document.docx).
* [Requirement Specifications](/projects/project-files/cyberminer/Requirement_Specification.docx).
* [Test Plan](/projects/project-files/cyberminer/Test_Plan.doc).
* [User manual](/projects/project-files/cyberminer/UserManual.docx).
* [Source Code](https://github.com/adeydas/Cyberminer).
