---
layout: page
title: Automated parking spot finder
permalink: /projects/automated-parking-spot-finder/
exclude_from_nav: true
---
The automated parking spot finder was an Arduino based system that notified on a web interface a grid of available parking spots categorized on permit levels. My responsibilities in this project included:

* Wrote code for the Arduino system to communicate the status of a spot to the central server.
* Designed and developed an API to "talk" with embedded devices to update parking spot status.
* Modified GUI to include parking lot switching.
* Developed an API using Twilio to receive texts and update the system based on the text content.

Below is a video demonstration of the working system.

<iframe width="420" height="315" src="https://www.youtube.com/embed/JuL5LIw7KDw" frameborder="0" allowfullscreen></iframe>

*Source code [#1](https://github.com/adeydas/cometparkwebsiteapi), [#2](https://github.com/adeydas/cometparksms)*