---
layout: page
title: Polygon Creator
permalink: /projects/polygon-creator/
exclude_from_nav: true
---
Often developers and analysts working on GIS applications require random polygons to test code. This application lets an user create a polygon on a map and export it as WKT for consumption.

![Polygon Creator](http://cdn.abhis.ws/cdn/site-images/polyc.png)

[Application here](http://polygoncreator.abhis.ws/)