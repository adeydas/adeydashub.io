---
layout: page
title: Projects
permalink: /projects/
---
**Academic projects**

* [Automated parking spot finder](/projects/automated-parking-spot-finder/)
* [Thought Search: A semantic search engine](/projects/thought-search/)
* [Monitor VM security in Openstack Cloud](/projects/monitor-vm-security-openstack-cloud/)
* [Cyberminer: A KWICK based search engine](/projects/cyberminer/)
* [Easy Clipboard manager](/projects/easy-clipboard-manager/)

**Professional projects (when employed)**

* Deployment platform for Pentaho Kettle transformations - Tango Analytics (May 2014 - Present)
* Spatial steps for Pentaho Kettle - Tango Analytics (May 2014 - Present)
* Distributed platform for disparate services - Tango Analytics (May 2014 - Present)
* R runner on the cloud - Tango Analytics (May 2014 - Present)
* Drivetime and Tradearea plugins for Alteryx - Tango Analytics (Jan 2014 - Apr 2014)
* Transmutation process for wire protocol - Amazon (May 2013 - Aug 2013)

**Professional projects (as a freelancer)**

* [Zelinx](https://zelinx.com)
* [Drop middle man](http://dropmiddleman.com)
* [Motiveminds CMS](http://motiveminds.com)
* [Website for Wingsoft Technologies LLC](http://www.wingsofttechnology.com/)

**Project-lets (*tiny projects that try to solve a problem*)**

* [Polygon Creator](/projects/polygon-creator/)
* [Kettle Code Generator](/projects/kettle-code-generator/)
* [Theft Detector](https://github.com/adeydas/Theft-Detector)
* [IP Keeper](https://github.com/adeydas/ipkeeper)