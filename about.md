---
layout: page
title: About Me
permalink: /about/
---

Hi, I am Abhishek, a geek from Kolkata, now in Dallas working as a Software Development Engineer for Amazon.

I am interested in algorithm design, implementation and distributed systems. During the day I work on the development and implementation of geospatial systems and during the night I switch to good old algorithm design apart from a myraid of other personal projects.


**About this site**

This site is made entirely with [Jekyll](http://jekyllrb.com), [Markdown](http://en.wikipedia.org/wiki/Markdown) and hosted on [Github](https://github.com/adeydas/adeydas.github.io). Feel free to re-use the source code except content in *_posts, projects, images, files and any file within the subdomain cdn.abhis.ws*.
The share buttons on blog post is courtesy of gschier, the source code to which is available on [Github](https://github.com/gschier/html-share-buttons).