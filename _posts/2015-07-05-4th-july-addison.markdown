---
layout: post
title:  "4th of July at Addison Circle"
date:   2015-07-05 23:37:00
categories: Misc
---
4th of July at the Addison Circle was a treat for the eyes. According to news reports about half a million people attended the Kaboom festival. The fireworks were amazing and to top it all, there was an air show as well. Here's a cell phone video of the final barrage of fireworks. Enjoy!

<iframe src="https://player.vimeo.com/video/135189293" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> <p><a href="https://vimeo.com/135189293">4th of July at Addison Circle</a> from <a href="https://vimeo.com/user28594289">Abhishek Dey Das</a> on <a href="https://vimeo.com">Vimeo</a>.</p> <p>Addison Circle lights up on the 4th of July!</p>