---
layout: post
title:  "Find all paths that lead to a target sum in a binary tree"
date:   2015-06-20 19:45:00
categories: algorithms
---
I found this interesting question in the book "Cracking the coding interview" recently. I believe a lot of similar questions appear in harder interviews and the solution(s) to it, for the lack of a better word, very interesting. So here goes:

> You are given a binary tree in which each node contains an integer value (which might be positive or negative). Design an algorithm to print all paths which sum to a given value. The path does not need to start or end at the root or a leaf, but it must go in a straight line down.

There are several ways to approach this problem and that is the beauty of a complicated problem. The first approach that came to my mind when I saw the question was top-down. That's how humans are designed to think, right? My algorithm went something like this:

* Take each node as a potential starting point and check for an available path where either 
  * sum is greater than target.
  * sum equals to target.
* If the former, print path.
* If latter, exit recursion stack.

Works, but not in the best possible way. Why? Take a look at the example below:

![Recurse tree](http://cdn.abhis.ws/cdn/blog-images/20-1.png)

For every node, it will try to get 'N' (N = number of nodes) possible paths that may or may not lead to the sum. True we could use some level of memoization here by storing previously computed values (see circled regions) but that's still 2^N calls.

**Building on that approach**

We can build on this approach a little more. Note here that our main concern is multiple calls from every node. How can we avoid that? Let's look at the bigger picture. What we are ultimately trying to do is this: take every path starting from the root to every single leaf and **drum roll** find a subset that equals to the target sum. Finding the subset is fairly easy. A simple recursive solution would be:

<script src="https://gist.github.com/adeydas/052b7d13f56104e2a805.js"></script>

Of course, it is highly NOT recommended to use this because of the repeated calls. A little bit of dynamic programming would go a long way here. As you probably would have found out by now the solution above is NP-Complete. So the best we could do here is find a pseudo-polynomial time solution.

**Thinking differently**

Let's go back to the first part of the solution. At every level we were looking for subsequent levels of nodes that could sum up to the target. What is we thought the other way round. What if we thought at every level whether that node makes up the sum.

![Recurse tree 2](http://cdn.abhis.ws/cdn/blog-images/20-2.png)

Say we got all the paths in each recursion stack. Now we can go though each level and ask ourselves: does it form a path that sums to target? In order words, use the already computed node values to find target. If we reach a node where sum equals target, we have found our destiny.

But then the question arises, how do we know the levels? Since, the path can only go down, we know for sure that number of levels for each recursion stack can be equal or lesser than the height of the tree and we know how to find the height.