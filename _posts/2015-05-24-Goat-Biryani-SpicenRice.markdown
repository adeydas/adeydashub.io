---
layout: post
title:  "Goat Biryani from Spice n Rice"
date:   2015-05-24 03:07:00
categories: Travel and Food
---
![Goat biryani from spice and rice]({{ "/images/spicenrice.jpg" | prepend: site.baseurl }})

If you love Lucknow or Kolkata style biryani, you have to give [Spice n Rice](http://spicenriceharun.com/) in Richardson a try. It's a refreshing move from the usual Hyderabadi biryani that you get in most Indian restaurants in and around Dallas. But that's not all. 

The goat biryani from Spice n Rice is a culmination of delicious spices, soft goat meat and potatoes. Priced at $10, it comes with a side of fresh salad and a copious amount of biryani. A must try if you are in or around Richardson.