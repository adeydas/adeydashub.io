---
layout: post
title:  "Moved to Jekyll and Github"
date:   2015-05-24 01:06:00
categories: Site updates
---
It's the inner geek that matters in the end. After a copious amount of time on blogging platforms like WordPress, MovableType and others, I finally decided to listen to the geek inside of me and moved to good and not so old Jekyll. So what are the advantages?

* **No more expensive hosting fees.** [Jekyll](http://jekyllrb.com) is a ruby gem that converts formatted blog posts to a static site. Github supports Jekyll and thus you can host your site on Github for a very expensive fee of $0.
* **Markdown support.** Jekyll supports markdown and textile out of the box. If you use git on a regular basis, you got to be familiar with markdown. In fact, I believe the simplicity of markdown is way better than many complicated WYSIWYG editors.
* **Highly customizable.** I am not saying other blogging platforms are not but Jekyll is downright easy to customize. In fact I got my site up and working in an hour with zero inital knowledge of Jekyll.
* **Syntax highlighting.** There are hardly any blog posts I would write that wouldn't require some sort of syntax highlighting. Jekyll supports both syntax highlighting and referecing gists out of the box. What more could you ask for?

There are several many reasons for you to swith to Jekyll and Github. These are the few that were important to me. I am excited to embark on a new blogging journey. Will keep posted on new discoveries. 