---
layout: post
title:  "Video log from San Antonio Sea World"
date:   2015-08-01 13:17:00
categories: Misc
---
Sea World at San Antonio, TX is a heaven for lovers of aquatic animals. Below are two amazing videos that I shot on my recent trip there.

<iframe src="https://player.vimeo.com/video/134635693" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> <p><a href="https://vimeo.com/134635693">Dance of Shamu at Sea World, San Antonio</a> from <a href="https://vimeo.com/user28594289">Abhishek Dey Das</a> on <a href="https://vimeo.com">Vimeo</a>.</p> <p>Shamu is an amazing killer whale who know how to shake as well. Watch the video if you don&#039;t believe me!</p>

<iframe src="https://player.vimeo.com/video/134646944" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> <p><a href="https://vimeo.com/134646944">Dolphins in all their joy</a> from <a href="https://vimeo.com/user28594289">Abhishek Dey Das</a> on <a href="https://vimeo.com">Vimeo</a>.</p> <p>Dolphins playing around to the tune of amazing music.</p>