---
layout: post
title:  "Posts on algorithms"
date:   2015-05-26 12:37:00
categories: algorithms
---
I decided to move my posts on algorithm design in to the same blog. However, since I post a lot in the particular category and not so much else where time wise, it wouldn't be appropriate if I had not separated the posts on algorithm design from other posts. Hence, I created this separate page with problems on algorithm design.

Since I am using Jekyll and Jekyll uses Liquid for template markups, writing code for doing all that was as easy as breeze. Below if the code on my homepage where every post other than posts in the algorithms category is shown.

<script src="https://gist.github.com/adeydas/8593b03c9dc3b6df0ba4.js"></script>
    
And the code for showing only posts on algorithms.

<script src="https://gist.github.com/adeydas/8e2ab734e0d11cf19f6e.js"></script>

Fun times!