---
layout: post
title:  "Allocating more resources to IntelliJ"
date:   2015-07-28 13:59:00
categories: Java
---
IntelliJ could run into issues if you have two many projects open or if your project is huge and requires a lot of memory. The most common issue is lag and higher load times. To prevent this open *{intellijdir}/bin/idea64.vmoptions* if you are using the 64-bit version or *{intellijdir}/bin/idea.vmoptions* if using the 32-bit version.

It's entirely up to you to set the configuration. Below is my configuration. Note that I have added *-XX:MaxHeapSize=4096m* to increase the heap size to ~4GB from the default value. The rest are modifications based on my requirements.

<script src="https://gist.github.com/adeydas/685357d7756d690493ec.js"></script>