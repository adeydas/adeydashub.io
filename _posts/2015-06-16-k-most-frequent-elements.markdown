---
layout: post
title:  "Find k most frequent elements in a stream of elements"
date:   2015-06-16 23:51:00
categories: algorithms
---
This (or a modification) is a very common question asked in tech interviews. 

**Find the k-most frequent elements in a stream of elements**

There are several solutions to it, most tutorials and articles prefer to use min heaps. That is no doubt a great solution and gives you the result in O(n) time. Here's one using Hashmaps with the same time complexity.

We need a count of the unique numbers that appear in the stream. For an operation like that we can use a Map with the element as the key and its count its corresponding value.

Once we do that we can either sort it (which would make the complexity nlgn) or apply an indirect approach. The second approach is to create an array of size of the frequency of the most frequent element. That would be one pass.

The third pass can be used to populate the array with *index=frequency of the element* and *value=the element*. Once that's done return the sub-array from the end to end-k.

[Here's the code](https://repo.abhis.ws/snippets/2).