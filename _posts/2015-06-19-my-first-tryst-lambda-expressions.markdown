---
layout: post
title:  "My first tryst with Lambda expressions"
date:   2015-06-19 13:43:00
categories: JAVA
---
Lambda expressions has been a fun little addition to JAVA 1.8. Unfortunately (for me) I never felt the need to use it until now. So what was I up to? I was trying to read all files in a directory and concatenate the content in a certain manner. True there are other ways to do it but doing it with Lambda expressions was uber cool. First let me throw down the code (because I am so excited about it) and then I will go into the awesomeness that is Lambda expressions.

<script src="https://gist.github.com/adeydas/105829f98d2d0daf7a09.js"></script>

While writing this small piece of code I came across quite a few interesting revelations that I would like to iterate here.

**Final and effectively final**

Yes they look so similar and no they are not the same. According to the Javadocs, an effectively final variable is one which does not change after assignment. But wait, isn't that what final is! Apparently so, the only difference is that you do not need to use the keyword final. If you do not perform an assignment after declaration, the Java compiler will consider that variable effectively final. 

**Cannot use 'non-final' local variables within Lambda expressions**

You heard it right. That's a big no-no. Instance variables are cool though.

I am sure there are other amazing stuff in it as well. Will post more when I find them. A brave new world awaits folks :-)