---
layout: post
title:  "My office away from office"
date:   2015-05-25 16:03:00
categories: Misc
---
![My rig]({{ "/images/IMG_1820.JPG" | prepend: site.baseurl }})
Upgraded my rig with a new UHD display. It's still running on a HDMI 1.4 connection but a Displayport 1.2 cable is on its way. Ah, love the screen real estate!