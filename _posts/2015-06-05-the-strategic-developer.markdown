---
layout: post
title:  "The strategic developer"
date:   2015-06-05 11:09:00
categories: misc
---
Came across this [blog post](http://www.infoworld.com/article/2614283/application-development/10-steps-to-becoming-the-developer-everyone-wants.html) by Andrew Oliver. He does put in some good points on what it takes to command respect and possibly a better salary. The part I liked the most was point #3. Having tunnel vision is the worst that could happen to a computer engineer because we need to think on a very abstract level. The ability to write good code is definitely a requirement but it doesn't supersede the ability to break down a problem and solve it bottom-up.